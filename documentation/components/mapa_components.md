# MAPA components

MAPA is a toolkit for detection and substitution of sensitive information in text.
Also, it is aimed to work with potentially any language, provided it is trained/configured 
properly.

In order to perform its task, MAPA relies on different components and approaches, which are 
configured and integrated into a simple web-service.

In general, a text anonymization process can be split in two main steps:
  - Sensitive information detection
  - Replacement of the sensitive information

MAPA addresses these two steps with a combination of different techniques.

## MAPA entity detection

The entity detection is the step in which the words bearing sensitive information are marked as such.

They might be names of people, or addresses, identification number, contact info, or any sort
of personal data. What is and what is not a sensitive piece of information depends on the 
specific needs of a particular use case.

MAPA combines two ways of detecting [sensitive] entities in a text.
One is using Artificial Intelligence models. The other is using configurable regular expressions.
They are complementary. While AI models are good at generalising and disambiguating entities based
on the context, the regular expressions are good to detect entities that follow reliable and
recognizable patterns, such as phone-numbers, email addresses, URLs, etc.

### Entity detection AI models

In MAPA the entity detection AI models are powered by the state-of-the-art Transformers models.
Transformers are Deep-Learning neural networks that, when trained of enough data for a given task,
are capable of achieving very good performance. One of the most well-known Transformer-based models
is Google's BERT model. MAPA makes use of BERT, more precisely the BERT multilingual pre-trained model,
which allows MAPA working in virtually any language for which there is enough training data.

#### The model (arch, training, etc.)

The architecture of the neural-network model that provides the AI-based entity detection is
the one described in the picture.

![alt text](../pictures/mapa_detection_model_arch.png "MAPA Anonymisation")
                                                                 

Given a text, it must be tokenized first. The tokenization process generates "tokens", which
are the unit of text on which the model with operate. A token can be a full word, a part of a word,
punctuation, etc.
The Transformers model receives the sequence of tokens, and works with them internally.
It is not the purpose of this explanation to describe how a Transformer model works internally,
but there are very illustrative explanations all around the web.

At the end, the Transformer model produces a contextual embedding for each input token.
An embedding is a vector of numbers that encodes information about the token. It is called
"contextual" because the information encoded is not only dependent on the input token itself,
but also depends on the whole context. These embeddings are the base on which to apply extra
neural-network layers to perform the classification task. In this case, the classification 
layers project each token-embedding to predict whether they are an entity (or not) and of
which type is the entity (among the possible types trained in the model). In the case of MAPA
there are two level of entities. Level 1 is more coarse-grained (e.g. PERSON), and level 2 is
more fine-grained (e.g. "person first name", "person last name").

![alt text](../pictures/mapa_detection_classif_head.png "MAPA Anonymisation")

The classification heads are just a stack of neural-network layers that receive and embedding
(which is a vector of size H) and projects it to an entity space of size N (assuming there are
N different possible entity labels to emit).

MAPA allows training entity-detection models if you have suitable labelled data, with the 
entities types you need. The resulting models can be used to launch the MAPA service (after
adding them in the configuration file).

### Regular-expressions based detection

To complement the AI based detection, MAPA allows configuring regular-expressions to detect
entities that follow recognisable patterns.

The regular-expressions can be configured in files following a very straightforward format.
Each file must contain lines with format LABEL::REGEXP, where LABEL is the label you want to
give to that entity type (e.g. phone-number) and REGEXP is the actual regular expression that
will be used to match occurrences in the input texts.
See a simple example [HERE](../../example_files/example_configuration/named_patterns.txt).

The REGEXP must follow Python [regular-expression
syntax](https://docs.python.org/3/library/re.html). There are online sites that help you to validate
Python-based regular expressions to ensure that they detect what you want (or you can go
through trial and error to adjust them). Regular expressions can be quite powerful, but can also
become very complex quickly. They should be used to detect entities that follow reasonably simple
patterns and that pose no ambiguity (regular expressions may over-detect things if the pattern
is too broad). For those cases it may be better to fall back to Artificial Intelligence, that
can generalize and rely on the context to disambiguate entities (however, you need training data
to teach the AI-model to detect what you want).


## MAPA entity replacement

Once the sensitive entities have been detected, they need to be replaced somehow. The objective
is to remove the sensitive information carried by the detected entities.

There are two different approaches to this: obfuscation and substitution.
                                               
### Entity obfuscation

Entity obfuscation is the simplest one. It basically consists of replacing every sensitive word
by a string of characters, be it 'X' or '*' (e.g. John Smith -> **** *****).
Despite being the simplest approach, it has obvious limitations. The resulting text becomes less
natural, difficult to read and may hinder additional processing.

MAPA allows for this kind of obfuscation. The obfuscation can be configured to operate only
on certain entity types (among the different entity types that can be detected in the
detection step), or it can be completely disabled.

### Entity substitution

Instead of simply obfuscating the sensitive information using an arbitrary symbol to replace
the original elements of the text, the entities can be replaced by other equivalent words.
The idea is to remove the original information (which was sensitive) and replace it by
synthetic data that resembles the original entities.

This entity-substitution approach is more elaborated, but leads to more readable texts. If the
replacements are done correctly, the resulting text should sound completely natural, and be
indistinguishable from an original text (except that the entities in the
substituted text are synthetic and do not refer to real sensitive information).
                                                                       
MAPA allows three different strategies to substitute an entity, that can be combined based
on the detected entity types: neural language model, random-character substitution and 
gazetteer-based substitution.

#### Neural Language Model based substitution

The neural language model (NLM) substitution approach, as its name suggests, makes use of a
language model to choose the correct substitution for a given word.
A language model is used to calculate the probability distribution over all the possible vocabulary
entries, given the context of the word we want to substitute. In particular, the way many 
Transformer models (such as BERT) are pre-trained is by using a Masked Language Model (MLM).
That is exactly the kind of thing we need: mask the word we want to substitute, and sample 
a new word from the most probable words according to a given language model.

In MAPA we use BERT multilingual as the language model, because it has knowledge of 104 
different languages, so we can use it for almost any language. Of course, depending on the 
language of the content and the content itself, the quality of the substitution may vary.
Other BERT-pretrained models can be used for specific languages in case they exist.
For example, we could use BETO (a BERT model pre-trained for Spanish) for Spanish texts.

Apart from the mere probability-based sampled, there are some additional rules and heuristics
to discard potentially invalid words, e.g. do not pick the very same word we want to replace,
replace capitalized words by words with the same casing, replace numbers by numbers, etc.


#### Random-character substitution

The random replacer is useful for elements that contain string of letter and/or numbers that
do not bear any specific information. For example passport numbers, or phone numbers that can
be replaced by any other random string of equivalent elements. The random-character substitution
replaces each character by another random character of the same type (e.g. digits by digits,
and letters by letters).

#### Gazetteer-based substitution

The gazetteer-based substitution allows using pre-configured name dictionaries to be used to
substitute specific entity types. For example, a list of female names can be used to replace
any entity detected and classified as "first name female". In a similar manner there can
be lists of names for cities, countries, surnames, organisations, etc. Those names can be
real or made up, they are just strings that will be sampled from configuration files.
The files can also be arranged by languages, so for example you can configure a list of
French names and another separated list of Spanish names, to be applied the texts depending 
on the detected language.

## MAPA configuration

All the explained mechanisms and approaches can be configured in the mapa_config.yml file that
is read by the service to operate. Here there is a
[CONFIGURATION FILE EXAMPLE](../../example_files/example_configuration/mapa_config.yaml) that can be adapted.

It contains several sections, each of them controlling different components.
It is quite a lot of information, so next we will decompose and explain each section.

```yaml
####################################
# MAPA anonymization configuration
####################################

# if a model needs to be downloaded (e.g. HuggingFace base models for entity_replacement) it will be cached there (it is a HF built-in mechanism)
cache_folder: /DATA/agarciap_data/python_stuff/pytorch-torchtext-data/CACHE
##############################
# DETECTION MODELS to be loaded, comment out the ones you don't want to load
##############################
# The entity detection/classification models listed here will be loaded.
# They are models trained with the MAPA trainer
# The models need to be in the same folder that this very file (the MAPA_BASE_FOLDER env var)
# If a model is not listed here, it will not be possible to use it when the service is launched
# Each listed model is composed of an arbitrary name (e.g. eurlex_all)
# and two parameters: the actual name of the model-folder, and the cuda_device in which it must be loaded
# cuda_device -1 means using CPU instead of GPU.
detection_models:
  eurlex_all:
    model_name: eurlex_ALL_v3_epoch124_iter109375_L1_miF-0.8374_L1_binF-0.8669_L2_miF-0.8467_loss-6.2376
    cuda_device: -1
  # legal_es:
    # model_name: ES_LEGAL+25+eurlexES_e2FL_v1_epoch53_iter3078_L1_miF-0.9551_L1_binF-0.9831_L2_miF-0.9532_loss-1.0528
    # cuda_device: -1
###############################
# REPLACEMENT MODELS to be loaded, comment out the ones you don't want to load
###############################
# Replacement models are the Neural Language Models to be used in the entity-substitution part.
# Again, the configuration consists of an arbitrary name (e.g. bert_multi) and the two parameters
# the actual name of the model (in this case its tag from HuggingFace models hub) and the cuda_device
# By default, it relies on multilingual BERT, but any other (canonical) BERT could be used
# for example BETO for Spanish.
replacement_models:
  bert_multi:
    model_name: bert-base-multilingual-cased
    cuda_device: -1
  # ixambert:
    # model_name: ixa-ehu/ixambert-base-cased
    # cuda_device: -1

#################################################
# STACKS of DETECTION + REPLACEMENT models, with a readable label that represents them
# Pointers to loaded model labels, comment out the stacks you want to omit
#################################################
# Finally, this is where the actual model stacks are configured combining detection models
# with substitution models
# You can use a French entity detection model combined with a multilingual entity substitution model
# Each combination consist of an arbitrary name that will act as the label to select the stack,
# plus the detection model and a replacement model (both of them must refer to the ones configured above)
# there is also a default stack, that acts as a fallback if the service does not receive any
# specific stack request in the corresponding parameter
default_anonymization_stack: eurlex_all+bert_multi
anonymization_stacks:
  eurlex_all+bert_multi:
    detection_model: eurlex_all
    replacement_model: bert_multi
  # legal_es+bert_multi:
    # detection_model: legal_es
    # replacement_model: bert_multi
  # legal_es+ixambert:
    # detection_model: legal_es
    # replacement_model: ixambert

#####################
# OPERATIONAL CONFIGURATION THAT CONTROLS THE ANALYSIS
#####################

#########################
# DEMO/SHOWCASE related config
#########################
# this just allows setting a pre-configured text in the simple front-end demo GUI
showcase:
  default_text_file: default_input_text.txt

##########################
# DETECTION related config
##########################
# The entity detection part has some parameters that can be configured
detection:
  # these parameters have to do with the AI-based model inference
  # they control how long the input sequences are internally, and the batch-size
  # do not touch them unless you know what you are doing
  sequence_len: 300
  context_len: 100
  batch_size: 4
  # the following parameters control the operation of the regular-expression pattern matching
  # The pattern-matching module can be totally disabled, or if enabled, here is where
  # you can point to the files (one or more) that contain the regular-expressions
  # (the paths are relative to the same folder in which this file is, the MAPA_BASE_FOLDER)
  pattern_matching:
    enabled: true
    regex_files:
      - named_patterns.txt

############################
# REPLACEMENT related config
############################
# Here is where the modules in charge of entity replacement are configured
replacement:
  # simple obfuscation (entity_replacement by '*' symbols)
  obfuscation:
    # the following list indicates which entity type to obfuscate
    entities_to_obfuscate:
      - given name
      - family name
      - ADDRESS
  # neural entity_replacement related configuration
  neural_replacer:
    # These parameters control the behaviour of the neural-language-model replacer
    batch_size: 8
    top_candidates: 10
    chunk_window_size: 30
    # languages for which only the head part of the word will be replaced (maybe suitable for morphologically rich languages, not tested)
    only_head_replacement_langs: []
    # entity types that will be replaced (take into account the two-levels of MAPA entities)
    # The neural-replacement will only apply to the entity-types mentioned here)
    replaceable_entity_types:
      - PERSON
      - family name
      - ADDRESS
      - month
      - year
      - day
    # as a general heuristic only entity-words starting by capital letter are replaced, except for entity types that appear in this list
    # in case you need certain lowercase words that appear within entities to be replaced
    lowercase_replaceable_entity_types:
      - month
    # stopwords will not be eligible as entity_replacement candidates
    # these words will never be sampled as possible replacements
    stopwords: ['el', 'la', 'de', 'a', 'del', 'le']
    # reserved words will not be eligible as replaceable
    # these words will never be replaced, even if they fall within an entity to be replaced
    reserved_words: ['DNI']
    # set a fixed random seed to make the sampling deterministic (suitable for experimentation)
    random_seed: -1
    # Combine the word-to-word similarity with the contextual-lm-probability
    # this is an advanced heuristic to bias the word-probability distribution
    use_non_contextual_sim_reweighting: True
    # The relevance of the contextual-lm-probability in the combination, i.e. contextual * alpha + non_contextual * (1 - alpha)
    contextual_reweighting_alpha: 0.5
    # Automatic Mixed Precision, no need to touch this
    use_amp: True
  # random character entity_replacement (for entities such as identifiers)
  random_replacer:
    # target entity types for this kind of entity_replacement
    # the entities listed here will be replaced using the random-replacer
    replaceable_entity_types:
      - DNI
    # words/tokens that will not be replaced even if they are matched as part of an entity
    # same as above, the words in this list will never be eligible for replacement
    reserved_words: ['DNI']
  # dictionary based entity_replacement, to pick names from predefined lists (not yet implemented, just a placeholder)
  gazetteer_replacer:
    # it can be totally disable, or configured only for some entities
    enabled: true
    # omit some particles ('del', 'van', 'von', etc.) to avoid weird combinations
    particles_to_omit: ['del']
    # language-based configuration (with a default fallback), picking names from specific files given the entity type
    # the default configuration will be the fall-back in case there is not language-specific
    # file configured for a certain entity type
    default_config:
      given name - male: en_male_names.txt
      given name - female: en_female_names.txt
      family name: en_family_names.txt
    language_config:
      # if there is a language-specific file for a given language it will be used
      # the choice of the language is based on an automatic language detection done by MAPA
      es:
        given name - male: es_male_names.txt
        given name - female: es_female_names.txt
        family name: es_family_names.txt

```





