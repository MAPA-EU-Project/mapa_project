## Detection by Regular Expression

In addition to the Deep Learning based detection, MAPA admits the configuration of regular expressions
as a mechanism to help to detect entities that follow fixed alphanumeric patterns, such as National 
ID numbers, emails, URLs, phone numbers, etc.
They can be detected by the Deep Learning models, but when they follow a simple and recognisable pattern
it is easier to describe that pattern using a regular-expression mechanism.

The regular expressions for MAPA are configured in one or more separated files. Then, these files are
indicated in the MAPA configuration file, in the corresponding section.
The format of the regex files is very simple.
They consist of lines, each containing pairs of labels and regular expressions (as interpreted 
by Python built-in regex module), separated by double-colon (::).
Empty lines or lines starting by hashtag (#) are ignored.

Se the following example:

```text
# The expected format is LABEL::REGEX
# One per line
# Empty lines or lines starting by '#' are ignored

DNI::\d{8}[A-Z]
PHONE::9\d{1,2}[\-\s]?(?:(?:\d{2})[\-\s]?){3}
IBAN::([a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[0-9]{7}([a-zA-Z0-9]?){0,16})
EMAIL::[\w\.]+@[\w\.]+\.[\w]+
```

Using a file like the above, all the text spans that match a certain regular expression would be 
tagged using the label that accompanies the matched expression.