import torch
from torch import nn
from transformers import BertTokenizer
from transformers.models.bert import BertForMaskedLM

models_cache_dir = "/DATA/agarciap_data/python_stuff/pytorch-torchtext-data/CACHE"
model_name = 'bert-base-multilingual-cased'
# model_name = 'ixa-ehu/ixambert-base-cased'

model = BertForMaskedLM.from_pretrained(model_name, cache_dir=models_cache_dir)

tokenizer = BertTokenizer.from_pretrained(model_name, cache_dir=models_cache_dir)
# some_indices = [146, 456, 765, 2342, 9876]
some_indices = tokenizer.convert_tokens_to_ids(tokenizer.tokenize('Barcelona marzo Rusia María Constitución Marzo'))

print('Some indices', some_indices, tokenizer.convert_ids_to_tokens(some_indices))

original_tokens_word_embs = model.bert.embeddings.word_embeddings(torch.tensor(some_indices))

print('original_tokens_word_embs shape', original_tokens_word_embs.shape)

bert_embeddings = model.bert.embeddings.word_embeddings.weight

print('bert_embeddings shape', bert_embeddings.shape)

# cos = nn.CosineSimilarity(dim=1, eps=1e-8)

# cos_dis = cos(bert_embeddings, original_tokens_word_embs)
cos_dis = torch.cdist(bert_embeddings, original_tokens_word_embs, p=2)
# cos_dis = cos(torch.transpose(original_tokens_word_embs, 1, 0), torch.transpose(bert_embeddings, 1, 0))

print('cos_dis shape', cos_dis.shape)
print('some cos_dis values', cos_dis[:2, :2])

firsts = torch.sort(cos_dis, dim=0)[1].T[:, :10].tolist()
print('raw first', firsts)
for candidates in firsts:
    back_tokens = tokenizer.convert_ids_to_tokens(candidates)
    print('First:', candidates)
    print('Back tokens:', back_tokens)


#############
# Z = original_tokens_word_embs
# B = bert_embeddings
# Z_norm = torch.linalg.norm(Z, dim=1, keepdim=True)  # Size (n, 1).
# B_norm = torch.linalg.norm(B, dim=0, keepdim=True)  # Size (1, b).
#
# # Distance matrix of size (b, n).
# cosine_similarity = ((Z @ B) / (Z_norm @ B_norm)).T
# cosine_distance = 1 - cosine_similarity
#
# first = cosine_distance.tolist()[0][:10]
# back_tokens = tokenizer.convert_ids_to_tokens(firsts)
# print('(quick) First:', firsts)
# print('(quick) Back tokens:', back_tokens)

def get_sim(candidate_embs, lm_vocab_embs):
    cos_dis = torch.cdist(lm_vocab_embs, candidate_embs, p=2)
    cos_sim = 1 - cos_dis
