import pytest

from mapa_project.entity_replacement.neural_lm_entity_replacement import NeuralLMEntityReplacerConfig, NeuralLMEntityReplacer


@pytest.fixture()
def neural_replacer():
    model_path = 'bert-base-multilingual-cased'
    # model_path = 'ixa-ehu/ixambert-base-cased'
    cache_dir = '/DATA/agarciap_data/python_stuff/pytorch-torchtext-data/CACHE'
    replaceable_entities = ['PER', 'LOC', 'day', 'month', 'year', 'DATE']
    lowercase_entities = ['month']

    config = NeuralLMEntityReplacerConfig(
        model_path=model_path,
        cache_dir=cache_dir,
        replaceable_entity_types=replaceable_entities,
        lowercase_replaceable_entity_types=lowercase_entities,
        batch_size=4,
        top_candidates=10,
        chunk_window_size=30,
        random_seed=42,
        cuda_device_num=-1,
        use_amp=True
    )
    replacer = NeuralLMEntityReplacer(replacer_config=config)
    return replacer


def _turn_example_into_token_tags(example: str):
    ws_tokens = example.split()
    tokens, labels = [], []
    for tok in ws_tokens:
        if '/' in tok:
            token, label = tok.split('/')
        else:
            token, label = tok, 'O'
        tokens.append(token)
        labels.append(label)
    return tokens, labels


def tests_neural_replacement(neural_replacer: NeuralLMEntityReplacer):
    example = 'El señor Perez/PER está en Barcelona/LOC desde el 3/day de/DATE abril/month de/DATE 2004/year .'
    # example = 'Frantziako/LOC hiriburua Paris/LOC da .'
    tokens, labels = _turn_example_into_token_tags(example)
    replaced_tokens = neural_replacer.replace_entities(tokens=tokens, labels=labels, replace_full_words=True)
    print()
    print('Original tokens:', tokens)
    print('Original labels:', labels)
    print('Replaced tokens:', replaced_tokens)
