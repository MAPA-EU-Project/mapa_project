

# An example of minimal parameters to launch a MAPA training (as provided in the mapa_final codebase)
# The XXXX and other placeholders must be replaced by valid values (paths) from the target environment
# You can execute mapa_train --help  in the console to see a list and description of the parameters

mapa_train  \
--model_name mapa_train_check \
--model_version 1 \
--pretrained_model_name_or_path 'bert-base-multilingual-cased' \
--models_cache_dir XXXXXXX \
--data_dir XXXXXXX \
--train_set "XXXXX_TRAIN.json" \
--dev_set "XXXXX_DEV.json" \
--checkpoints_folder XXXXXX/checkpoints \
--batch_size 6 \
--cuda_devices "-1" \
--max_checkpoints_to_keep 5