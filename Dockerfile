FROM python:3.7.9-slim-buster

####################################
# To enable CUDA from the container (at least on our machines, it might be different in other settings)
ENV PATH /usr/local/cuda/bin/:$PATH
ENV LD_LIBRARY_PATH /usr/local/cuda/lib:/usr/local/cuda/lib64
ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES compute,utility
LABEL com.nvidia.volumes.needed="nvidia_driver"
#####################################

# Choose among: 1.7.0+cpu , 1.7.0+cu101 or simply 1.7.0   (for CPU, CUDA10.1 and CUDA10.2 respectively)
ARG PYTORCH_VERSION=1.7.0+cu101

RUN pip install --no-cache-dir torch==$PYTORCH_VERSION -f https://download.pytorch.org/whl/torch_stable.html
COPY ./requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

ARG SERVICE_PORT_ARG=8668
ENV SERVICE_PORT=$SERVICE_PORT_ARG

EXPOSE $SERVICE_PORT

# CUDA DEVICE, -1 means CPU  (note: CUDA must be available, otherwise CPU will be used)
ENV CUDA_DEVICE_NUM=-1

COPY mapa_project ./mapa_project

#CMD ["sh", "-c", "uvicorn mapa_project.webservice.main:_app --host 0.0.0.0 --port $SERVICE_PORT"]
CMD ["sh", "-c", "python -m mapa_project.webservice.main"]