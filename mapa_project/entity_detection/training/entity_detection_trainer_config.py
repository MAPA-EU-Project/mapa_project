import logging
import sys
from pathlib import Path
from typing import Optional, Union

from pydantic import BaseModel, Field
from pydantic_cli import to_runner

from mapa_project import __version__

logger = logging.getLogger(__name__)


class MapaEntityDetectionTrainerConfig(BaseModel):
    model_name: str = Field(..., description='The name of the trained model')
    model_version: int = Field(..., description='The version number of the model')
    model_description: Optional[str] = Field(description='An optional description of the model')

    pretrained_model_name_or_path: Union[str] = Field(..., description='The name or path to the pretrained base model')
    pretrained_tokenizer_name_or_path: Optional[str] = Field(default=None, description='Name/path to pretrained tokenizer, '
                                                                                       'only in case it differs from the model name/path')
    do_lower_case: bool = Field(default=False, description='Force the tokenizer\'s do_lower_case to True')
    models_cache_dir: str = Field(..., description='Cache directory to store downloaded models')

    data_dir: Union[Path, str] = Field(..., description='Directory containing the train/dev data')
    train_set: str = Field(..., description='Name of the training data file')
    dev_set: str = Field(..., description='Name of the validation (a.k.a dev) data file')

    checkpoints_folder: Union[Path, str] = Field(..., description='Directory to store the model checkpoints during the training')
    max_checkpoints_to_keep: Optional[int] = Field(None, description='Number of checkpoints to keep during training (the oldest ones are removed)')

    num_epochs: int = Field(200, description='The maximum number of epochs to train')
    early_stopping_patience: int = Field(50, description='Number of epochs without improvement before early-stopping the training')

    batch_size: int = Field(..., description='The batch size (number of examples processed at once) during the training')

    train_iters_to_eval: int = Field(default=-1, description='The number of iterations before triggering an evaluation (apart from full epochs)')

    gradients_accumulation_steps: int = Field(default=1, description='The number of steps to accumulate gradients before an optimizer step')
    clip_grad_norm: float = Field(default=1.0, description='The value for gradient clipping')

    lr: float = Field(2e-05, description='The learning rate for the training')
    warmup_epochs: float = Field(default=1., description='The number of epochs to warmup the learning rate. Admits non-integer values, e.g. 0.5')

    cuda_devices: str = Field(default='-1', description='Comma-separated CUDA device numbers to train on GPU (-1 means CPU)')

    amp: bool = Field(default=True, description='The use of Automatic Mixed Precision (amp) to leverage fp16 operations and speed-up GPU training')

    random_seed: int = Field(default=42, description='The random seed, to make the experiments reproducible')

    valid_seq_len: int = Field(default=300, description='The length of the sliding-window (the central valid part excluding the contexts)')
    ctx_len: int = Field(default=100, description='The length of the context surrounding each sliding-window')
    # this assumes a BERT model as the base Transformers, it is ok
    labels_to_omit: str = Field(default='O,X,[CLS],[SEP],[PAD]', description='The labels omitted from the in-training evaluation (default for BERT)')

    @classmethod
    def parse_config_from_console(cls, exit_on_error: bool = True) -> 'MapaEntityDetectionTrainerConfig':
        class ConfigCatcher:

            def __init__(self):
                self.parsed_config: Optional[MapaEntityDetectionTrainerConfig] = None

            def catch_config(self):
                def launch(config: MapaEntityDetectionTrainerConfig) -> int:
                    self.parsed_config = config
                    return 0

                def handle_exceptions(exception: BaseException) -> int:
                    logger.error(f'{exception}')
                    return 0

                to_runner(cls, launch, version=__version__, exception_handler=handle_exceptions)(sys.argv[1:])
                return self.parsed_config

        parsed_config = ConfigCatcher().catch_config()
        if not parsed_config and exit_on_error:
            sys.exit(1)
        return parsed_config

    def pretty_print_config(self):
        params = []
        for arg in vars(self):
            params.append(f'{arg}: {getattr(self, arg)}')
        return '\n' \
               '==================================================\n' \
               '  Experiment configuration and hyper-parameters:' \
               '\n' \
               '==================================================\n  ' + '\n  '.join(params) + \
               '\n================================================'
