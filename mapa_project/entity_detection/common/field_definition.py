"""
Definition of input/output fields, basically to hold vocabulary creation automation and store some other valuable information for each field
"""
import json
import logging
import os
from collections import Counter
from copy import deepcopy
from dataclasses import dataclass, field
from typing import List, Dict, Union, ClassVar, Optional

from dataclasses_serialization.json import JSONSerializer
from transformers import PreTrainedTokenizer

logger = logging.getLogger(__name__)


@dataclass
class FieldDefinition:
    STORE_NAME_APPENDIX: ClassVar[str] = '_vocabulary'

    name: str
    is_sequence: bool
    is_multilabel: bool
    pad_token: Optional[str]
    default_value: Optional[str]
    cls_token: Optional[str]
    sep_token: Optional[str]
    bos_token: Optional[str]
    eos_token: Optional[str]
    special_tokens: List[str] = field(default_factory=list)
    vocab: Optional[Dict[str, int]] = None
    reverse_vocab: Optional[Dict[int, str]] = None

    def __post_init__(self):
        self.__refresh_special_tokens()
        if self.vocab:
            self.reverse_vocab = {x: y for y, x in self.vocab.items()}

    def __refresh_special_tokens(self):
        special_tokens = [self.pad_token, self.default_value, self.cls_token, self.sep_token, self.bos_token, self.eos_token]
        self.special_tokens: List[str] = [special_token for special_token in special_tokens if special_token is not None]

    @classmethod
    def create_non_sequential_field(cls, name: str, multilabel: bool, default_label: str = None,
                                    vocabulary: Dict[str, int] = None) -> 'FieldDefinition':
        return cls.__create_field(name=name,
                                  is_sequence=False,
                                  is_multilabel=multilabel,
                                  tokenizer=None,
                                  default_label=default_label,
                                  vocab=vocabulary)

    @classmethod
    def create_sequential_field(cls, name: str, multilabel: bool, tokenizer: PreTrainedTokenizer = None, default_label: str = None,
                                vocabulary: Dict[str, int] = None) -> 'FieldDefinition':
        return cls.__create_field(name=name,
                                  is_sequence=True,
                                  is_multilabel=multilabel,
                                  tokenizer=tokenizer,
                                  default_label=default_label,
                                  vocab=vocabulary)

    @classmethod
    def inherit_from_tokenizer(cls, name: str, tokenizer: PreTrainedTokenizer) -> 'FieldDefinition':
        return cls.__create_field(name=name,
                                  is_sequence=True,
                                  is_multilabel=False,
                                  tokenizer=tokenizer,
                                  vocab=tokenizer.get_vocab())

    @classmethod
    def __create_field(cls, name: str, is_sequence: bool, is_multilabel: bool, tokenizer: PreTrainedTokenizer = None, default_label: str = None,
                       vocab: Dict[str, int] = None) -> 'FieldDefinition':
        is_tokenizer_verbose = tokenizer.verbose if tokenizer else False
        # Note than being verbose means being not-None, so the access to the tokenizer should be safe
        if is_tokenizer_verbose:
            tokenizer.verbose = False
        default_label = default_label or (tokenizer.unk_token if tokenizer else None)
        # NOTE the extra check to prevent 'None' string literals, due to a little bug in Transformers 4.4.1 (I have reported it)
        field_definition = FieldDefinition(name=name,
                                           is_sequence=is_sequence,
                                           is_multilabel=is_multilabel,
                                           pad_token=tokenizer.pad_token if tokenizer and tokenizer.pad_token != 'None' else None,
                                           default_value=default_label,
                                           cls_token=tokenizer.cls_token if tokenizer and tokenizer.cls_token != 'None' else None,
                                           sep_token=tokenizer.sep_token if tokenizer and tokenizer.sep_token != 'None' else None,
                                           bos_token=tokenizer.bos_token if tokenizer and tokenizer.bos_token != 'None' else None,
                                           eos_token=tokenizer.eos_token if tokenizer and tokenizer.eos_token != 'None' else None,
                                           vocab=vocab)
        if is_tokenizer_verbose:
            tokenizer.verbose = True
        return field_definition

    def attn_mask_name(self) -> str:
        """ An optional name to use for the mask version of the field, to ease naming consistency """
        return '{}_attn_mask'.format(self.name)

    def ctx_mask_name(self) -> str:
        """ An optional name to use for the context mask version of the field (for sliding windows), to ease naming consistency """
        return '{}_ctx_mask'.format(self.name)

    def pad_idx(self) -> int:
        if self.vocab:
            return self.stoi(self.pad_token)
        else:
            raise Exception(f'Field {self.name} vocabulary not provided/created before requesting pad_idx')

    def unk_idx(self) -> int:
        if self.vocab:
            return self.stoi(self.default_value)
        else:
            raise Exception(f'Field {self.name} vocabulary not provided/created before requesting unk_idx')

    def build_labels_vocab(self, instances: List[Dict[str, Union[str, List[str]]]] = None, direct_vocab: List[str] = None,
                           min_support: Union[int, float] = 1,
                           default_counts_for_min_support: bool = True):
        """
        Builds the labels vocab from input instances, usually from the train set.
        Note that if min_support>1, then values will be pruned, and then a default_value for them will be necessary.
        :param instances: instances from which to build the vocabulary
        :param direct_vocab: a vocabulary (list of labels) to be directly injected (this is mutually exclusive with instances)
        :param min_support: the minimum support of a value/label in the instances to keep it from pruning
        :param default_counts_for_min_support: if the default label counts for the total label mass count when min_support is a float value
        :return: 
        """""
        # 3 main cases: classification, multi-label classification, sequence-labelling (both single-label or multi-label)

        if self.is_sequence:
            # build sequence-labelling vocabulary (expects labels as csv strings)
            self.__build_sequence_labelling_vocabulary(instances, direct_vocab, self.default_value, min_support, default_counts_for_min_support)
        else:
            # non-sequence (i.e. document classification)  (expects labels as a single string, or as a list of strings)
            self.__build_classification_vocabulary(instances, direct_vocab, self.default_value, min_support, default_counts_for_min_support)

    def __init_vocab_with_special_tokens(self, default_value: str, is_sequential_field: bool) -> Dict[str, int]:
        self.__update_unk_token(default_value)
        vocab: Dict[str, int] = dict()
        if is_sequential_field:
            # add the special tokens (note: the default value will act as the unk_token)
            if self.pad_token:
                vocab[self.pad_token] = len(vocab)
            else:
                logger.warning(f'WARNING: no pad_token for field {self.name} (this might be ok if it is the desired behaviour)')
            for special_token in self.special_tokens:
                if special_token not in vocab:
                    # important to check if it already is to prevent errors
                    vocab[special_token] = len(vocab)
        else:
            # non-sequential fields (i.e. for document classification) should only need default value, not pad, cls, sep nor other special value
            if self.default_value:
                vocab[self.default_value] = len(vocab)
        return vocab

    def __add_vocab(self, vocab: Dict[str, int]):
        self.vocab = vocab
        self.reverse_vocab = {x: y for y, x in self.vocab.items()}

    def __build_sequence_labelling_vocabulary(self, instances: List[Dict[str, Union[str, List[str]]]], direct_vocab: List[str],
                                              default_value: str, min_support: Union[int, float], default_counts_for_min_support: bool = True):
        vocab = self.__init_vocab_with_special_tokens(default_value=default_value, is_sequential_field=True)

        if instances and direct_vocab:
            raise Exception('Instances and direct_vocab cannot be provided at the same time, they are mutually exclusive')
        elif instances:
            vocab = self.__fill_vocab_from_sequence_labelling_instances(vocab=vocab, instances=instances, min_support=min_support,
                                                                        default_counts_for_min_support=default_counts_for_min_support)
        elif direct_vocab:
            logger.info(f'Using direct_vocab {direct_vocab} for field: {self.name}')
            for elem in direct_vocab:
                if elem not in vocab:
                    vocab[elem] = len(vocab)
        else:
            raise Exception('Instances or direct_vocab must be provided to build a vocabulary')
        # instantiate the vocabulary
        self.__add_vocab(vocab=vocab)

    def __fill_vocab_from_sequence_labelling_instances(self, vocab: Dict[str, int], instances: List[Dict[str, Union[str, List[str]]]],
                                                       min_support: Union[int, float], default_counts_for_min_support: bool = True):
        # count frequencies to apply min_support
        counter = Counter()
        for instance in instances:
            # for sequence labelling field content is known to be a list of values (the sequence)
            field_content: List[str] = instance[self.name]
            for value in field_content:
                if self.is_multilabel:
                    # multi-label, value holds comma-separated values
                    for csv_value in value.split(','):
                        counter[csv_value] += 1
                else:
                    # single-label, the usual way, full value gets accounted
                    counter[value] += 1
        # retain only the values with a frequency greater or equal to min_support
        values_with_min_support = self.__filter_by_min_support(label_counter=counter, min_support=min_support,
                                                               default_counts_for_min_support=default_counts_for_min_support)
        for value in values_with_min_support:
            if value not in vocab:
                # important to check if it already is to prevent errors
                vocab[value] = len(vocab)
        return vocab

    def __build_classification_vocabulary(self, instances: List[Dict[str, Union[str, List[str]]]], direct_vocab: List[str],
                                          default_value: str, min_support: Union[int, float], default_counts_for_min_support: bool):
        vocab = self.__init_vocab_with_special_tokens(default_value=default_value, is_sequential_field=False)
        if instances and direct_vocab:
            raise Exception('Instances and direct_vocab cannot be provided at the same time, they are mutually exclusive')
        elif instances:
            vocab = self.__fill_vocab_from_classification_instances(vocab=vocab, instances=instances, min_support=min_support,
                                                                    default_counts_for_min_support=default_counts_for_min_support)
        elif direct_vocab:
            logger.info(f'Using direct_vocab {direct_vocab} for field: {self.name}')
            for elem in direct_vocab:
                if elem not in vocab:
                    vocab[elem] = len(vocab)
        else:
            raise Exception('Instances or direct_vocab must be provided to build a vocabulary')
        # instantiate the vocabulary
        self.__add_vocab(vocab=vocab)

    def __fill_vocab_from_classification_instances(self, vocab: Dict[str, int], instances: List[Dict[str, List[str]]], min_support: Union[int, float],
                                                   default_counts_for_min_support: bool):
        # count frequencies to apply min_support
        counter = Counter()
        for instance in instances:
            # for multi-label classification field content is known to be a list of values (the multiple labels)
            field_content: Union[str, List[str]] = instance[self.name]
            if isinstance(field_content, list):
                for value in field_content:
                    counter[value] += 1
            elif isinstance(field_content, str):
                counter[field_content] += 1

        # retain only the values with a frequency greater or equal to min_support
        values_with_min_support = self.__filter_by_min_support(label_counter=counter, min_support=min_support,
                                                               default_counts_for_min_support=default_counts_for_min_support)
        for value in values_with_min_support:
            if value not in vocab:
                # important to check if it already is to prevent errors
                vocab[value] = len(vocab)
        return vocab

    def __filter_by_min_support(self, label_counter: Counter, min_support: Union[int, float], default_counts_for_min_support: bool) -> List:
        """
        Return a filtered label list accoring to min_support.
        min_support can be an integer (absolute value) or a [0-1] float, meaning a ratio of the total label count.
        """
        # check min_support type (and validity of the value)
        if min_support is None:
            return list(label_counter.keys())  # if no min_support provided, return keys as-is, unfiltered
        elif isinstance(min_support, int):
            if min_support < 0:
                raise Exception(f'Integer min_support must be greater or equal to zero, provided value: {min_support}')
            values_with_min_support = [x for x, y in label_counter.most_common() if y >= min_support]
            return values_with_min_support
        elif isinstance(min_support, float):
            if min_support < 0. or min_support > 1.:
                raise Exception(f'Float min_support must be in range [0-1], provided value: {min_support}')
            total_labels = sum(label_counter.values())
            if self.default_value and self.default_value in label_counter and not default_counts_for_min_support:
                # if there is a default label and it must not account for the total label mass, remove its count from the total
                total_labels = total_labels - label_counter[self.default_value]
            values_with_min_support = [x for x, y in label_counter.most_common() if y / total_labels >= min_support]
            return values_with_min_support

    def __update_unk_token(self, new_default: str):
        """ The fields are created with the tokenizer's unk_token if no a priori vocabulary was available. When generating vocabulary update it """
        self.default_value = new_default
        self.__refresh_special_tokens()

    def stoi(self, value: str) -> int:
        """ stoi stands for string-to-int, returns the index of a given vocabulary entry """
        if value in self.vocab:
            return self.vocab[value]
        elif self.default_value is not None:
            return self.vocab[self.default_value]
        else:
            raise Exception(f'Non-existent vocabulary value:{value} requested for a vocabulary without default value.')

    def itos(self, index: int) -> str:
        """ itos stands for int-to-string, returns the vocabulary entry for a given index """
        if index in self.reverse_vocab:
            return self.reverse_vocab[index]
        else:
            raise Exception(f'Non-existent index value:{index} requested for a vocabulary.')

    def serialize_to_file(self, base_path: str, field_name: str):
        self_copy = deepcopy(self)
        self_copy.reverse_vocab = None  # remove the reverse_vocab to avoid serializing redundant information
        serializable_dict = JSONSerializer.serialize(self_copy)
        path = os.path.join(base_path, f'{field_name}{self.STORE_NAME_APPENDIX}.json')
        with open(path, 'w', encoding='utf-8') as f:
            f.write(json.dumps(serializable_dict))

    @classmethod
    def deserialize_from_file(cls, base_path, field_name: str) -> 'FieldDefinition':
        path = os.path.join(base_path, f'{field_name}{cls.STORE_NAME_APPENDIX}.json')
        with open(path, 'r', encoding='utf-8') as f:
            deserialized_dict = json.loads(f.read())
        return JSONSerializer.deserialize(cls, deserialized_dict)
