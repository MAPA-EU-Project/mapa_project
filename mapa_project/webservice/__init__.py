import logging

LOGGING_FORMAT = '%(name)s - %(levelname)s - %(message)s'  # config.get('LOGGING', 'LOGGING_FORMAT', vars=os.environ)
LOGGING_LEVEL = 'INFO'  # just a provisional value immediately to be changed by the loaded configuration

logging.basicConfig(format=LOGGING_FORMAT, level=LOGGING_LEVEL)

logger = logging.getLogger(__name__)

logger.info(f'Basic logger configured')
