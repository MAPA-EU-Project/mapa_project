import logging
from typing import Dict, Callable, Optional

from watchdog.events import FileSystemEventHandler
from watchdog.observers.api import ObservedWatch
from watchdog.observers import Observer

logger = logging.getLogger(__name__)


class ConfigFileChangeEventHandler(FileSystemEventHandler):

    def __init__(self, operation: Callable):
        super().__init__()
        self.operation = operation

    def on_modified(self, event):
        super().on_modified(event)
        # There is a problem when we use WinSCP to open and edit remote files, it first creates an empty file, which causes an error
        self.operation()


class FileWatchManager:

    def __init__(self, operation: Callable):
        self.watched_stuff_map: Dict[str, ObservedWatch] = {}
        self.observer = Observer()
        self.event_handler = ConfigFileChangeEventHandler(operation=operation)
        self.observer.start()

    def __exit__(self, exc_type, exc_value, traceback):
        self.observer.stop()
        self.observer.join()

    def start_watching(self, path):
        watch: ObservedWatch = self.observer.schedule(self.event_handler, path, recursive=False)
        self.watched_stuff_map[path] = watch

    def stop_watching(self, path):
        if path in self.watched_stuff_map:
            watch = self.watched_stuff_map.pop(path)
            self.observer.unschedule(watch)


_config_file_watch_manager: Optional[FileWatchManager] = None


def init_or_get_config_file_watch_manager(operation: Optional[Callable] = None):
    # load only once
    global _config_file_watch_manager
    if _config_file_watch_manager is None:
        if operation is None:
            raise Exception('Trying to init config file watch without a reload operation')
        logger.info(f'Going to load config file watch manager...')
        _config_file_watch_manager = FileWatchManager(operation=operation)
        logger.info(f'Config file watch manager loaded...')
    return _config_file_watch_manager
