import os.path
import sys
from argparse import ArgumentParser

import confuse
from confuse import Configuration

from mapa_project.data_processing.jsonlines_files_generation import tsv_dataset_to_json
from mapa_project.entity_detection.evaluation.mapa_detection_evaluator import MapaEntityDetectorEvaluator
from mapa_project.entity_detection.helpers.systematic_training import SystematicTrainingRunning
from mapa_project.entity_detection.inference.entities_detection_inferencer import MAPATwoFlatLevelsInferencer
from mapa_project.entity_detection.training.entity_detection_trainer import MapaEntityDetectionTrainer
from mapa_project.entity_detection.training.entity_detection_trainer_config import MapaEntityDetectionTrainerConfig


def mapa_entity_detection_training_main():
    # assuming that sys.argv comes with the content it should
    training_config = MapaEntityDetectionTrainerConfig.parse_config_from_console(exit_on_error=True)
    trainer = MapaEntityDetectionTrainer(config=training_config)
    trainer.train()


def mapa_entity_detection_training_using_config_file_main():
    # assuming that sys.argv comes with the content it should
    argparse = ArgumentParser('MAPA training using YAML config file instead of passing the arguments via console', add_help=True)
    argparse.add_argument('--config', type=str, required=False, help='Path the YAML file that contains the configuration for the systematic training')
    argparse.add_argument('--train_args_help', action='store_true', required=False,
                          help='Pass the help flag to the trainer to show the arguments description')

    args = argparse.parse_args()
    if args.train_args_help:
        sys.argv += ['--help']
        print('*************************************************************')
        print('>> GOING TO SHOW THE ACTUAL TRAINING ARGUMENTS DESCRIPTION <<')
        print('*************************************************************\n')
        training_config = MapaEntityDetectionTrainerConfig.parse_config_from_console(exit_on_error=True)
    else:
        if not args.config:
            raise Exception('Missing --config argument')
        config_file = args.config
        yaml_training_config: Configuration = confuse.Configuration('MAPA single training', __name__)
        yaml_training_config.set_file(config_file)
        training_config = MapaEntityDetectionTrainerConfig(**yaml_training_config.get(dict))

    trainer = MapaEntityDetectionTrainer(config=training_config)
    trainer.train()


def mapa_entity_detection_systematic_train_main():
    """ The entry point to launch several pre-configured trainings in a batch, instead of one by one """
    argparse = ArgumentParser('MAPA systematic training', add_help=True)
    argparse.add_argument('--config', type=str, required=True, help='Path the YAML file that contains the configuration for the systematic training')
    argparse.add_argument('--cuda_devices', type=str, required=True, help='Comma-separated values of GPUs to use. The trainings will be rotated'
                                                                          'among the GPUs, e.g. 0,1,2 will perform trainings on GPU 0, 1 and 2')
    args = argparse.parse_args()
    yaml_config = args.config
    gpus = [int(device) for device in args.cuda_devices.split(',')]
    SystematicTrainingRunning(yaml_config_file=yaml_config).run_trainings(gpus=gpus)


def mapa_entity_detection_evaluation_main():
    argparse = ArgumentParser('MAPA detection evaluation', add_help=True)
    argparse.add_argument('--model_path', type=str, required=True, help='Path to the trained MAPA detection model to be used')
    argparse.add_argument('--test_set_path', type=str, required=True,
                          help='Path to the test_set file (NOTE: it refers to the already converted test_set to a single file)')
    # argparse.add_argument('--output_path', type=str, help='File path to write the evaluation results')
    argparse.add_argument('--cuda_device', type=int, default=-1, help='Number of CUDA device to use (-1 means CPU)')
    argparse.add_argument('--batch_size', type=int, default=8, help='The batch size during inference for evaluation (defaults to 8)')

    args = argparse.parse_args()

    inferencer = MAPATwoFlatLevelsInferencer(model_path=args.model_path,
                                             valid_seq_len=300, ctx_window_len=100,
                                             batch_size=args.batch_size,
                                             cuda_device_num=args.cuda_device)

    evaluator = MapaEntityDetectorEvaluator(inferencer=inferencer)
    evaluator.evaluate(test_data_path=args.test_set_path)
    # TODO: writing to output is missing yet (but not really necessary)
    # NOT that easy... we are using the CoNLL legacy script that directly prints to stdout, we need to refactor it to capture the output
    # output_path = args.output_path
    # if output_path:
    #     with open(output_path, 'w', encoding='utf-8') as f:
    #         # f.write()
    #         pass


def mapa_entity_detector_infer_main():
    argparse = ArgumentParser('MAPA detection inference', add_help=True)
    argparse.add_argument('--model_path', type=str, required=True, help='Path to the trained MAPA detection model to be used')
    argparse.add_argument('--cuda_device', type=int, default=-1, help='Number of CUDA device to use (-1 means CPU)')
    argparse.add_argument('--input_folder', type=str, required=True, help='Folder containing raw txt documents or json-lines documents')
    # argparse.add_argument('--encoding', type=str, default='utf-8', help='The encoding of the input files (e.g. utf-8, iso-8859-1, ...)')
    argparse.add_argument('--output_folder', type=str, required=True,
                          help='Folder that will contain the resulting documents in tsv format ??')  # TODO: check this once it is decided and done
    argparse.add_argument('--batch_size', type=int, default=8, help='The batch size during inference for evaluation (defaults to 8)')

    args = argparse.parse_args()

    inferencer = MAPATwoFlatLevelsInferencer(model_path=args.model_path,
                                             valid_seq_len=300, ctx_window_len=100,
                                             batch_size=args.batch_size,
                                             cuda_device_num=args.cuda_device)
    # TODO: Finish this
    # we assume a valid jsonlines file? Or what? What do we want?
    # Mmm... we can assume a txt as input, and as the output, we can try a tsv style file, with tokens, BIO-tag labels, and token spans
    # The input can also be jsonlines files, even with gold labels (no, not "even with", they must have them if we expect to an evaluable output
    # Ok, so json-lines are just for evaluation, are required gold-labels on them? Better to make it optional just in case
    # CASES: simple inference (not for external eval): input: txt or jsonlines, output, CoNLL-like format
    #        inference for eval: input: jsonlines with golds, output, CoNLL-like format


def mapa_tsv_to_json_conversion_main():
    argparse = ArgumentParser('MAPA conversion from Inception TSV to JSONLINES', add_help=True)
    argparse.add_argument('--train_folders', type=str, required=False, help='Paths to folders with TRAIN TSV files (if more than one, separate '
                                                                            'them by comma ",")')
    argparse.add_argument('--dev_folders', required=False, type=str,
                          help='Paths to folders with DEV TSV files (if more than one, separate '
                               'them by comma ",")')
    argparse.add_argument('--test_folders', required=False, type=str,
                          help='Paths to folders with TEST TSV files (if more than one, separate '
                               'them by comma ",")')
    argparse.add_argument('--output_folder', type=str, required=True, help='Target output folder to save the converted files')
    argparse.add_argument('--output_basename', type=str, required=True, help='The basename for the set of converted files'
                                                                             ' (they will be appended with TRAIN, DEV or TEST to differentiate them)')

    args = argparse.parse_args()

    train_folders = args.train_folders.split(',') if args.train_folders else []
    dev_folders = args.dev_folders.split(',') if args.dev_folders else []
    test_folders = args.test_folders.split(',') if args.test_folders else []
    target_folder = args.output_folder
    target_base_name = args.output_basename
    tsv_dataset_to_json(source_train_folders=train_folders, source_dev_folders=dev_folders, source_test_folders=test_folders,
                        target_folder=target_folder, target_base_name=target_base_name)

    print(f'Converted jsonlines files written to: {os.path.abspath(target_folder)}')
