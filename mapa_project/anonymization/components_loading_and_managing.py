import logging
import os
from typing import ClassVar, Dict

import confuse
from confuse import Configuration

from mapa_project.entity_detection.mapa_entity_detection import PatternMatchingConfig, EntityDetectionConfig, MapaEntityDetector
from mapa_project.anonymization.mapa_anonymization import MapaAnonymizer
from mapa_project.entity_replacement.mapa_entity_obfuscation import MapaEntityObfuscator, EntityObfuscationConfig
from mapa_project.entity_replacement.mapa_entity_replacement import EntityReplacementConfig, MapaEntityReplacer

# from watchdog.events import FileSystemEventHandler
# from watchdog.observers.api import ObservedWatch
# from watchdog.observers import Observer
from mapa_project.webservice.filewatch_manager import init_or_get_config_file_watch_manager

logger = logging.getLogger(__name__)


# class FileWatchManager:
#
#     def __init__(self, event_handler: FileSystemEventHandler):
#         self.watched_stuff_map: Dict[str, ObservedWatch] = {}
#         self.observer = Observer()
#         self.event_handler = event_handler
#         self.observer.start()
#
#     def __exit__(self, exc_type, exc_value, traceback):
#         self.observer.stop()
#         self.observer.join()
#
#     def start_watching(self, path):
#         watch: ObservedWatch = self.observer.schedule(self.event_handler, path, recursive=False)
#         self.watched_stuff_map[path] = watch
#
#     def stop_watching(self, path):
#         if path in self.watched_stuff_map:
#             watch = self.watched_stuff_map.pop(path)
#             self.observer.unschedule(watch)


class LoadedComponentsManager:
    MAPA_CONFIG_FILE_NAME: ClassVar[str] = 'mapa_config.yaml'

    MAPA_BASE_FOLDER_ENV_VAR: ClassVar[str] = 'MAPA_BASE_FOLDER'
    MAPA_PORT_ENV_VAR: ClassVar[str] = 'MAPA_PORT'

    def __init__(self):
        self.base_folder, self.mapa_port = self.__load_env_vars()
        self.mapa_config: Configuration = confuse.Configuration('MAPA', __name__)
        self.configuration_file = os.path.join(self.base_folder, self.MAPA_CONFIG_FILE_NAME)
        logger.info(f'Loading configuration from {os.path.abspath(self.configuration_file)}')
        self.mapa_config.set_file(self.configuration_file)
        self.loaded_anonymizers: Dict[str, MapaAnonymizer] = self.__load_anonymizers(self.mapa_config)
        self.default_anonymizer_label: str = self.mapa_config['default_anonymization_stack'].get(str)
        if self.default_anonymizer_label not in self.loaded_anonymizers:
            raise Exception(f'Default anonymizer label ({self.default_anonymizer_label}) '
                            f'not among the loaded anonymizers: {self.loaded_anonymizers.keys()}')
        self.default_demo_text = self.__load_demo_default_text()

        # The initialisation of the watchdog must happen here, because we need the instance to point to the function
        # At the same time, we tell it to watch the main config file (still missing the watches over other pointed files, TBD in each module)
        init_or_get_config_file_watch_manager(self.hot_reload_config).start_watching(self.configuration_file)

    def __load_env_vars(self):
        mapa_base_folder = os.environ.get(self.MAPA_BASE_FOLDER_ENV_VAR, None)
        mapa_port = os.environ.get(self.MAPA_PORT_ENV_VAR, None)
        if not mapa_port or not mapa_base_folder:
            raise Exception(f'{self.MAPA_BASE_FOLDER_ENV_VAR} and {self.MAPA_PORT_ENV_VAR} environment variables must be set')
        logger.info(f'Configured MAPA_BASE_FOLDER={mapa_base_folder} ; Configured MAPA_PORT={mapa_port}')
        return mapa_base_folder, int(mapa_port)

    def __load_demo_default_text(self):
        default_text_file = self.mapa_config['showcase']['default_text_file'].get(str)
        text = ''
        if default_text_file:
            default_text_file_path = os.path.join(self.base_folder, default_text_file)
            with open(default_text_file_path, 'r', encoding='utf-8') as f:
                text = f.read()
        return text

    def __load_anonymizers(self, config: Configuration) -> Dict[str, MapaAnonymizer]:
        pattern_matching_config = PatternMatchingConfig.load_from_config(self.base_folder, config)
        loaded_entity_detectors: Dict[str, MapaEntityDetector] = {
            label: MapaEntityDetector(entity_detector_config, pattern_matching_config)
            for label, entity_detector_config in EntityDetectionConfig.load_from_config(
                base_folder=self.base_folder, config=config).items()
        }

        loaded_entity_replacers: Dict[str, MapaEntityReplacer] = {
            label: MapaEntityReplacer(entity_replacer_config)
            for label, entity_replacer_config in EntityReplacementConfig.load_from_config(base_folder=self.base_folder, config=config).items()
        }
        entity_obfuscator = MapaEntityObfuscator(EntityObfuscationConfig.load_from_config(config))

        loaded_anonymizers: Dict[str, MapaAnonymizer] = {}
        anonymization_stacks_info = config['anonymization_stacks'].get(dict)
        for stack_label, stack_info in anonymization_stacks_info.items():
            detection_label = stack_info['detection_model']
            replacer_label = stack_info['replacement_model']
            loaded_anonymizers[stack_label] = MapaAnonymizer(
                entity_detector=loaded_entity_detectors[detection_label],
                entity_replacer=loaded_entity_replacers[replacer_label],
                entity_obfuscator=entity_obfuscator
            )
        return loaded_anonymizers

    def get_anonymizer(self, anonymizer_label) -> MapaAnonymizer:
        anonymizer_label = anonymizer_label or self.default_anonymizer_label
        return self.loaded_anonymizers[anonymizer_label]

    def hot_reload_config(self):
        try:
            self.mapa_config: Configuration = confuse.Configuration('MAPA', __name__)
            configuration_file = os.path.join(self.base_folder, self.MAPA_CONFIG_FILE_NAME)
            logger.info(f'Loading configuration from {os.path.abspath(configuration_file)}')
            self.mapa_config.set_file(configuration_file)
            for anomymizer_label, anonymizer in self.loaded_anonymizers.items():
                logger.info(f'Hot-reloading {anomymizer_label} config...')
                anonymizer.update_operational_config(config=self.mapa_config, base_folder=self.base_folder)
        except Exception as ex:
            # this is to prevent crashes when the edition is of a remote file (e.g. WinSCP first creates an empty file, and that causes a crash)
            # Another option would be to check if the new config file is empty before starting the reload
            # Further, it would be good to keep the previous config to restore it in case the new one is ill defined
            # But that would also apply to all the pointed files, and would be rather complex (this is poorly designed to achieve that)
            logger.error(f'Error hot-reloading config (may happen once when editing remote files)... {ex}')
