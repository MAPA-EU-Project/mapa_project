"""
Check the predictions generator (for external evaluation)
"""
from mapa_project.entity_detection.common.entity_detection_model import EnhancedTwoFlatLevelsSequenceLabellingModel
from mapa_project.entity_detection.evaluation.custom_conll_gen_for_eval import ModelDescriptors
from mapa_project.entity_detection.evaluation.mapa_predictions_generator import MapaPredictionsGenerator
from mapa_project.entity_detection.inference.entities_detection_inferencer import MAPATwoFlatLevelsInferencer

if __name__ == '__main__':
    # A folder with several jsonlines test files (with gold labels)
    input_folder = '/DATA/agarciap_data/MAPA_STUFF/MAPA_EXPERIMENTS/MAPA_MEDDOCAN/ES_DATA/test'
    output_folder = '/DATA/agarciap_data/MAPA_STUFF/MAPA_FINAL/tests/test_generation_meddocan_for_eval2/'

    model_path = '/DATA/agarciap_data/MAPA_STUFF/MAPA_FINAL/tests/checkpoints/' \
                 'ES_MEDDOCAN_0.20_test_v1_epoch8_iter288_L1_miF-0.8563_L1_binF-0.8698_L2_miF-0.8229_loss-0.0686/'

    inferencer = MAPATwoFlatLevelsInferencer(model_path=model_path, valid_seq_len=300, ctx_window_len=100, batch_size=5, cuda_device_num=-1,
                                             model_class=EnhancedTwoFlatLevelsSequenceLabellingModel, mask_level2=True)

    model_descriptors = ModelDescriptors(dataset_name='MEDDOCAN', dataset_domain='MEDICAL', dataset_lang='es',
                                         pretrained_model_lang='multi', cased=True, training_seed=42, mapa_model_arch='mapa_V1',
                                         tagging_scheme='IOB')

    generator = MapaPredictionsGenerator(inferencer=inferencer, model_descriptors=model_descriptors)
    generator.generate_predictions_from_inception_tsv(input_folder=input_folder, output_folder=output_folder)
