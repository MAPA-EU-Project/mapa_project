"""
Train the best legal_es model (legal_corpus + eurlex_es)
"""
from mapa_project.entity_detection.training.entity_detection_trainer import MapaEntityDetectionTrainer
from mapa_project.entity_detection.training.entity_detection_trainer_config import MapaEntityDetectionTrainerConfig

if __name__ == '__main__':
    # Let's check it
    config = MapaEntityDetectionTrainerConfig(
        model_name='FR_Clinical_ClinicalCases',
        model_version=6,
        model_description="A test with FR clinical data",
        # pretrained_model_name_or_path='/DATA/agarciap_data/MAPA_STUFF/MAPA_EXPERIMENTS/bert-base-multilingual-cased_vocab_extended',
        pretrained_model_name_or_path='flaubert/flaubert_base_cased',
        models_cache_dir='/DATA/agarciap_data/python_stuff/pytorch-torchtext-data/CACHE',
        data_dir='/DATA/agarciap_data/MAPA_STUFF/MAPA_EXPERIMENTS/FROM_VICTORIA/TRANSFORMED_DATA/',
        train_set='FR_Eurlex+CLINICAL_CASES_TRAIN.json',
        dev_set='FR_CLINICAL_CASES_DEV.json',
        checkpoints_folder='/DATA/agarciap_data/MAPA_STUFF/MAPA_FINAL/tests/checkpoints',
        # experiment_logdir='/DATA/agarciap_data/MAPA_STUFF/MAPA_EXPERIMENTS/LEGAL_CORPUS/RAW_76k_DICTAMENES_ONA/logdir_dtv3',
        num_epochs=200,
        early_stopping_patience=50,
        cuda_devices='6',
        batch_size=8,
        train_iters_to_eval=-1,
        lr=2e-5,
        valid_seq_len=300,
        ctx_len=100,
        warmup_epochs=5,
        amp=True,
        max_checkpoints_to_keep=5,
        # labels_to_omit='O,X,[SEP],[CLS],[PAD]'
        labels_to_omit='O,X,</s>,<s>,<pad>'  # note the different labels for flaubert...
    )
    trainer = MapaEntityDetectionTrainer(config=config)

    trainer.train()
