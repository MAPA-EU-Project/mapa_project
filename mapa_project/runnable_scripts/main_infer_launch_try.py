from typing import List

from transformers import PreTrainedTokenizer

from mapa_project.entity_detection.common.entity_detection_model import EnhancedTwoFlatLevelsSequenceLabellingModel
from mapa_project.entity_detection.common.pretokenization_helpers import GeneralPreTokenizer
from mapa_project.entity_detection.inference.entities_detection_inferencer import MAPATwoFlatLevelsInferencer, MapaSeqLabellingOutput

if __name__ == '__main__':
    model_path = '/DATA/agarciap_data/MAPA_STUFF/MAPA_FINAL/tests/checkpoints/' \
                 'ES_LEGAL+25+eurlexES_e2FL_v1_epoch17_iter900_L1_miF-0.9421_L1_binF-0.9590_L2_miF-0.9396_loss-0.0663/'

    inferencer = MAPATwoFlatLevelsInferencer(model_path=model_path, valid_seq_len=300, ctx_window_len=100, batch_size=5, cuda_device_num=-1,
                                             model_class=EnhancedTwoFlatLevelsSequenceLabellingModel, mask_level2=True)
    # TODO: There is some BUG in the level2_mask application, BUT ALSO, note how DATE gets affected (forcing a L2 label for the stopwords)
    # That is probably the origin of that noise in the "2 de mayo" being "de" tagged as a "month" or similar.
    # What a mess... it needs a rework and a check, we need to ensure that the masking is doing its work, or simply remove it.
    # input_example = """
    #     Petición de decisión prejudicial planteada por el Arbeitsgericht Cottbus — Kammern Senftenberg ( Alemania ) el 2 de mayo de 2018 — Reiner Grafe y Jürgen Pohle
    # """
    # input_example = """
    #     El Sr . Pedro Ramírez , originario de Fuenlabrada , Madrid , dice que no estuvo en el paseo de Gracia el 4 de agosto .
    # """
    # input_example = """
    #     El otro día la señora Pilar Etxeberria estuvo recorriendo la calle Mayor de Burgos, y el 4 de agosto de 2024 cumple 10 años de casada.
    # """
    # input_example = """
    #     La señora Pilar Etxeberria.
    # """
    # input_example = """
    #     F'dawk il-każijiet l-applikanti applikaw għal pensjoni Awstrija wara l-isħubija u talbu li ċerti fatti li seħħew fi Stati Membri oħra qabel l-isħubija jittieħdu in konsiderazzjoni (li fil-każ Kauer kienu jikkonċernaw il-perjodu ta' trobbija ta' tifel fil-Belġju u, fil-każ Duchon, inċident li seħħ il-Ġermanja u li rrriżulta f'mankament).
    # """
    input_example = """
    Dña. María del Mar Pérez Rubio, procuradora de los tribunales, en nombre y representación de D. José Antonio Sánchez Pujol, ante este juzgado comparece y como mejor en derecho proceda, dice
Que interesa, conforme al art. 781.1 de la LECrim, la apertura de juicio oral ante el juzgado de lo penal, contra Marcos Torres Prada, de conformidad con las siguientes conclusiones provisionales.
El pasado día 3 de junio de 2008, en la zona de discotecas de Sant Andreu, de la calle San Ramón Nonato, Marcos Torres Prada, sin antecedentes penales, junto con el menor en ese momento Javier Gómez Padilla y otras personas, agredieron a José Antonio Sánchez Pujol.
"""

    pretokenizer = GeneralPreTokenizer(prepend_whitespaces=False, remove_linebreaks=False)

    pretokenized_input = [pretokenizer.do_pretokenization(raw_text=input_example.strip(), calculate_offsets=False)]

    results: List[MapaSeqLabellingOutput] = inferencer.make_inference(pretokenized_input=pretokenized_input)

    all_res = []
    for i, mapa_result in enumerate(results):
        print(f'lens: {len(mapa_result.tokens)}, {len(mapa_result.level1_tags)}, {len(mapa_result.level2_tags)}')
        print(f'tokens ({len(mapa_result.tokens)})', mapa_result.tokens)
        print(f'labels ({len(mapa_result.level1_tags)})', mapa_result.level1_tags)
        print(f'labels ({len(mapa_result.level2_tags)})', mapa_result.level2_tags)
        res = []
        for j, token in enumerate(mapa_result.tokens):
            level1_label = mapa_result.level1_tags[j]
            # level1_label = level1_label if level1_label != '[UNK]' else 'O'
            level2_label = mapa_result.level2_tags[j]
            # level2_label = level2_label if level2_label != '[UNK]' else 'O'
            res.append(f'{token}/{level1_label}::{level2_label}')
        all_res.append(res)

    for res in all_res:
        print(' '.join(res))

    #############
    tokenizer: PreTrainedTokenizer = inferencer.tokenizer
    for i, mapa_result in enumerate(results):
        for j, token in enumerate(mapa_result.tokens):
            bert_tokens = tokenizer.tokenize(token)
            level1_label = mapa_result.level1_tags[j]
            # level1_label = level1_label if level1_label != '[UNK]' else 'O'
            level2_label = mapa_result.level2_tags[j]
            # level2_label = level2_label if level2_label != '[UNK]' else 'O'

            print(f'>> token: {token} btokens: {bert_tokens} L1: {level1_label} L2: {level2_label}')