"""
From the parsed tsv information, build actionable jsonline files for the trainings.
I prefer to extract all this logic here and not to pollute the tsv parsing logic, which might need extension or improvement later

What do we want?

Level 1 annotation as a BIO tagging
Level 2 annotation as a another independent BIO tagging

Do we want local classifiers, or just two classifiers (mixing all the 2nd level in the second layer?)
The non-sense pairings (incorrect child) predictions could be filtered out to permit only valid combinations
E.g. a mask over the 2nd level positions that do not make sense for a 1st level prediction, and then an argmax over the non-masked positions
Sounds cool... only that during training such information will not be available, I mean, the softmax and loss calculation will be over all of them.

So, in summary, if we try such an approach, we just need two simple layers/levels. Nothing fancy, just two outputs.
As a first approach sounds nice. We are missing the information for a proper BIO tagging though...
I have them now :-)

"""
import json
import os
from typing import List, Dict

# CAREFUL: has the whitespaces thing here interact somehow with the white_space stuff below? does it have any impact?
from tqdm import tqdm

from mapa_project.data_processing.mapa_tsv_parsing import InceptionTokenAnnotation, InceptionDocumentAnnotation, CharOffset, LabelAnnotation, \
    parse_folders_with_inception_tsv_files, PARSING_ERRORS, parse_inception_file
from mapa_project.entity_detection.common.pretokenization_helpers import GeneralPreTokenizer

PRETOKENIZER = GeneralPreTokenizer(prepend_whitespaces=False, remove_linebreaks=False, add_linebreak_tokens=True)


def further_retokenize_inception_tokens_(parsed_document: InceptionDocumentAnnotation):
    """ Inception tokens may contain punctuation joining what should end up being several pretokens, process them"""
    for sent_num, sentence in enumerate(parsed_document.sentences):
        retokenized_annotations: List[InceptionTokenAnnotation] = []
        for token_annon in sentence.tokens_annotations:
            text = token_annon.text
            offset = token_annon.offset
            level1_label = token_annon.level1_label
            level2_label = token_annon.level2_label
            # token_num = token_annon.token_num # token nums are all shifted according to how many new tokens are obtained
            pretokens = PRETOKENIZER.do_pretokenization(raw_text=text, calculate_offsets=False)
            current_offset: int = offset.start
            for pretoken in pretokens:
                retokenized_annotation = InceptionTokenAnnotation(sentence_num=sent_num, token_num=len(retokenized_annotations),
                                                                  offset=CharOffset(current_offset, current_offset + len(pretoken)), text=pretoken,
                                                                  level1_label=level1_label, level2_label=level2_label)
                current_offset += len(pretoken)
                retokenized_annotations.append(retokenized_annotation)
        sentence.tokens_annotations = retokenized_annotations


def convert_parsed_document_to_sequence_labelling(parsed_document: InceptionDocumentAnnotation, retokenize_tokens: bool) -> Dict[str, List[str]]:
    """ We want the input tokens, and the two levels of labels, including their BIO tagging """

    if retokenize_tokens:
        # print('RETOKENIZING INCEPTION TOKENS TO OBTAIN A SUITABLE TOKENIZATION...')
        further_retokenize_inception_tokens_(parsed_document=parsed_document)
    else:
        print('WARNING: NO RETOKENIZATION IS BEING PERFORMED')

    input_tokens: List[str] = []
    level1_tags: List[str] = []
    level2_tags: List[str] = []

    for sentence in parsed_document.sentences:
        last_level1_label_id = -1
        last_level2_label_id = -1
        for token_num, token in enumerate(sentence.tokens_annotations):
            token_text = filter_unicode_characters(token.text)

            if token_text != '':  # after filtering if may end up empty, in such case skip it
                preceded_by_whitespace = __was_preceded_by_white_space(token, sentence.tokens_annotations[token_num - 1] if token_num > 0 else None)
                input_tokens.append(token_text if not preceded_by_whitespace else ' ' + token_text)
                last_level1_label_id = add_bio_tagged_label(last_level1_label_id, level1_tags, token.level1_label)
                last_level2_label_id = add_bio_tagged_label(last_level2_label_id, level2_tags, token.level2_label)

    return {'tokens': input_tokens, 'level1_tags': level1_tags, 'level2_tags': level2_tags}


def __was_preceded_by_white_space(current_token: InceptionTokenAnnotation, prev_token: InceptionTokenAnnotation):
    # conditions:
    #  - there is a prev_token (i.e. not the first token)
    #  - previous token is not empty (even after filtering out unicode whitespaces)
    #  - previous (non-empty) token ends exactly where the current one starts
    # if prev_token and filter_unicode_characters(prev_token.text) != '' and prev_token.offset.end + 1 < current_token.offset.start:
    #     return False
    if prev_token and filter_unicode_characters(prev_token.text) != '' and prev_token.offset.end == current_token.offset.start:
        return False
    else:
        return True


def filter_unicode_characters(text):
    # so far this is only for the NBSP Latin-1 -> \xa0 which since is a white-space is uninteresting for BERT
    # Note that for any Sentence-Piece tokenization based model it is relevant, but so are the character offsets, or at least all the white-spaces
    # For such cases we would need a slightly different conversion logic
    text = text.replace(u'\xa0', u' ')
    return text.strip()


def add_bio_tagged_label(last_level_label_id: int, level_tags: List[str], label: LabelAnnotation) -> int:
    level_tag = generate_bio_label(last_level_label_id, label)
    level_tags.append(level_tag)
    return label.id


def generate_bio_label(last_level_label_id: int, label: LabelAnnotation):
    if label != LabelAnnotation.empty_annotation():
        level_tag = f'B-{label.label}' if label.id != last_level_label_id else f'I-{label.label}'
    else:
        level_tag = label.label  # the O label
    return level_tag


def bulk_convert_tsv_to_jsonlines(input_folders: List[str], output_path, retokenize_tokens: bool):
    """ Read tsv files from a bunch of folders, convert them, and write down altogether to the output path"""
    parsed_documents = parse_folders_with_inception_tsv_files(folders=input_folders)

    seq_labelling_formatted_instances = []
    for parsed_document in parsed_documents:
        seq_labelling_formatted_instances.append(
            convert_parsed_document_to_sequence_labelling(parsed_document=parsed_document, retokenize_tokens=retokenize_tokens))

    with open(output_path, 'w', encoding='utf-8') as f:
        f.writelines([json.dumps(instance) + '\n' for instance in seq_labelling_formatted_instances])

    print(f'Jsonlines result written to: {os.path.abspath(output_path)}')


def convert_single_tsv_file_to_jsonlines(tsv_file_path: str, base_output_path: str, retokenize_tokens: bool):
    parsed_document = parse_inception_file(input_path=tsv_file_path)
    # just one instance per document (for now there is no way to consistently split a document into meaningful fragments)
    seq_labelling_instance = convert_parsed_document_to_sequence_labelling(parsed_document=parsed_document, retokenize_tokens=retokenize_tokens)
    output_path = os.path.join(base_output_path, os.path.basename(tsv_file_path) + '.jsonl')
    print(f'Writing individual test file to {os.path.abspath(output_path)}')
    with open(output_path, 'w', encoding='utf-8') as f:
        f.writelines(json.dumps(seq_labelling_instance) + '\n')


def convert_tsv_folder_into_individual_jsonlines(input_folder: str, output_folder: str, retokenize_tokens: bool):
    """ Read tsv files from a bunch of folders, convert them, and write down altogether to the output path"""
    files = [os.path.join(input_folder, file) for file in os.listdir(input_folder) if file.endswith('.tsv')]
    for file in tqdm(files):
        parsed_document = parse_inception_file(input_path=file)
        instance = convert_parsed_document_to_sequence_labelling(parsed_document=parsed_document, retokenize_tokens=retokenize_tokens)
        output_path = os.path.join(output_folder, os.path.basename(file) + '.jsonl')
        with open(output_path, 'w', encoding='utf-8') as f:
            f.writelines([json.dumps(instance) + '\n'])
        # print(f'Jsonlines result written to: {os.path.abspath(output_path)}')


def tsv_dataset_to_json(source_train_folders: List[str], source_dev_folders: List[str], source_test_folders: List[str],
                        target_folder: str, target_base_name: str):
    if source_train_folders:
        target_train_path = os.path.join(target_folder, target_base_name + '_TRAIN.json')
        bulk_convert_tsv_to_jsonlines(input_folders=source_train_folders, output_path=target_train_path, retokenize_tokens=True)
    if source_dev_folders:
        target_dev_path = os.path.join(target_folder, target_base_name + '_DEV.json')
        bulk_convert_tsv_to_jsonlines(input_folders=source_dev_folders, output_path=target_dev_path, retokenize_tokens=True)
    if source_test_folders:
        target_test_path = os.path.join(target_folder, target_base_name + '_TEST.json')
        bulk_convert_tsv_to_jsonlines(input_folders=source_test_folders, output_path=target_test_path, retokenize_tokens=True)

    print('### PARSING ERRORS ENCOUNTERED ###')
    [print(f' >>> {error}') for error in PARSING_ERRORS]
    print(f'{len(PARSING_ERRORS)} parsing errors')
