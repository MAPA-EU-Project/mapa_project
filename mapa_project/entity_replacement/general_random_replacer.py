"""
Logic to make substitution according to certain rules with regard to the entity type, or to how it is composed (numbers, letter, punctuation marks...)
"""
import logging
import random

import string
from collections import Set
from dataclasses import dataclass, field
from typing import List

from confuse import Configuration

from mapa_project.anonymization.datatypes_definition import Tokens, Labels
from mapa_project.anonymization.helpers import strip_bio_tagging

logger = logging.getLogger(__name__)


@dataclass
class GeneralRandomReplacerConfig:
    replaceable_entities: List[str]
    reserved_tokens: List[str] = field(default_factory={'DNI', 'CIF', 'NIE', 'www', 'http', 'https', 'com', 'es', 'eu', 'org', 'info'})

    @classmethod
    def load_from_config(cls, config: Configuration) -> 'GeneralRandomReplacerConfig':
        random_replacer_info = config['replacement']['random_replacer']
        return GeneralRandomReplacerConfig(
            replaceable_entities=random_replacer_info['replaceable_entity_types'].get(list),
            reserved_tokens=random_replacer_info['reserved_words'].get(list)
        )


class GeneralRandomizedReplacer:

    def __init__(self, config: GeneralRandomReplacerConfig):
        self.config = config
        self.replaceable_entities: Set[str] = set(config.replaceable_entities)
        self.reserved_tokens: Set[str] = set(config.reserved_tokens)

    def update_operational_config(self, config: Configuration):
        """ A method that further propagates (or applies) the configuration changes that can be updated on-the-fly """
        replacer_config = GeneralRandomReplacerConfig.load_from_config(config)
        self.replaceable_entities: Set[str] = set(replacer_config.replaceable_entities)
        self.reserved_tokens: Set[str] = set(replacer_config.reserved_tokens)

    def replace_entities(self, tokens: Tokens, level1_labels: Labels, level2_labels: Labels) -> Tokens:
        replaced_tokens = [self.__replace_token(token) if self.__is_replacement_target(level1_labels[i], level2_labels[i]) else token for i, token in
                           enumerate(tokens)]
        return replaced_tokens

    def __is_replacement_target(self, level1_label: str, level2_label: str) -> bool:
        level1_label_wo_bio = strip_bio_tagging(level1_label)
        level2_label_wo_bio = strip_bio_tagging(level2_label)
        return level1_label_wo_bio in self.replaceable_entities or level2_label_wo_bio in self.replaceable_entities

    def __replace_token(self, token: str) -> str:
        if token in self.config.reserved_tokens:
            return token
        replaced_token_chars = []
        for c in token:
            if c.isalpha():
                replaced_token_chars.append(random.sample(string.ascii_uppercase if c.isupper() else string.ascii_lowercase, 1)[0])
            elif c.isnumeric():
                replaced_token_chars.append(random.sample(string.digits, 1)[0])
            else:
                replaced_token_chars.append(c)
        resulting_token = ''.join(replaced_token_chars)
        logger.info(f'Randomly replacing "{token}" by "{resulting_token}"')
        return resulting_token
